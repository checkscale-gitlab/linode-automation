# postgres-server

This will deploy a Debian 9 instance with Postgres 11 installed from the official Postgres repository (not the build that comes with Debian).  The system is configured to allow SSH and Postgres access only on the private IP address, a Postgres user and database are created, and several common extensions created on the database.

Before using this, your secrets must be set in Ansible Vault encrypted files as shown below.

```bash
cd postgres-server
ansible-vault create terraform-secrets.json
# Insert contents of terraform-secrets.json-template and edit.
ansible-vault create postgres-install-secrets.yaml
# Insert contents of postgres-install-secrets.yaml-template and edit.
```

There is also the following configuration file which contains no secrets.

```bash
cp terraform.tfvars-template terraform.tfvars
# Edit terraform.tfvars.
```

And finally edit the iptables rules.

```bash
cp upload/iptables-rules.v4-template upload/iptables-rules.v4
# Edit upload/iptables-rules.v4 to add your administrative IP for SSH access.
```

Now you can deploy a new Postgres server with the following commands.

```bash
terraform init
terraform plan
terraform apply
```

You now should be able to login to Postgres as follows, with private-ip being the IP reported by `terraform apply` above, and dbuser and dbname being the database name and username that you specified in postgres-install-secrets.yaml.

```bash
psql -h private-ip -U dbuser dbname
```
