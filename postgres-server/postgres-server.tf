# Author Lyle Frost <lfrost@cnz.com>

data "external" "secrets" {
    program = ["ansible-vault", "view", "terraform-secrets.json"]
}

provider "linode" {
    token = "${data.external.secrets.result.token}"
}

resource "linode_instance" "postgres1" {
    image           = "linode/debian9"
    label           = "postgres1"
    group           = "Database"
    region          = "${var.region}"
    type            = "${lookup(var.types, var.type)}"
    authorized_keys = "${var.authorized_keys}"
    root_pass       = "${data.external.secrets.result.root_pass}"
    swap_size       = 512
    private_ip      = true

    connection {
        type        = "ssh"
        user        = "root"
        host        = "${linode_instance.postgres1.private_ip_address}"
        private_key = "${file("~/.ssh/id_ed25519")}"
    }

    # Install all updates and set up iptables.
    provisioner "remote-exec" {
        inline = [
            "apt-get update",
            "apt-get upgrade --assume-yes",
            "apt-get install --assume-yes net-tools python3-psycopg2 rsync",
            "echo 'iptables-persistent iptables-persistent/autosave_v4 boolean true' | debconf-set-selections",
            "echo 'iptables-persistent iptables-persistent/autosave_v6 boolean true' | debconf-set-selections",
            "apt-get install --assume-yes iptables-persistent",
            "mv /etc/iptables/rules.v4 /etc/iptables/rules.v4-original",
            "mv /etc/iptables/rules.v6 /etc/iptables/rules.v6-original"
        ]
    }

    provisioner "file" {
        source      = "upload/iptables-rules.v4"
        destination = "/etc/iptables/rules.v4"
    }

    provisioner "file" {
        source      = "upload/iptables-rules.v6"
        destination = "/etc/iptables/rules.v6"
    }

    provisioner "remote-exec" {
        inline = [
            "iptables-restore /etc/iptables/rules.v4",
            "ip6tables-restore /etc/iptables/rules.v6"
        ]
    }

    # Configure the new instance.
    provisioner "local-exec" {
        command = "ansible-playbook -i ${linode_instance.postgres1.private_ip_address}, -u root postgres-install.yaml --extra-vars 'ansible_python_interpreter=/usr/bin/python3 private_ip=${linode_instance.postgres1.private_ip_address}'"
    }
}
